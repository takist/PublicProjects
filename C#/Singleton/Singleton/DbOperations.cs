﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Singleton
{
    public sealed class DbOperations
    {
        // Thread-safe implemantation of singleton

        // Explicit static constructor to tell C# compiler  
        // not to mark type as beforefieldinit  
        private static readonly DbOperations instance = new DbOperations("DB connection string");

        static DbOperations()
        {

        }

        private DbOperations(string x)
        {
            Console.WriteLine($"{x}");
        }

        public static DbOperations Instance
        {
            get
            {
                return instance;
            }
        }
    }
}
