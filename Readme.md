Created by takist,

| Pattern | C# | Python | Explanation |
| --- | ------ | ------ | ------------|
| **Singleton pattern/** | x | x | Protect the fact that you have one object to operate, can't create new object on runtime (keeps already created object) |
| **Factory pattern/** | | ~ | Same as builder, more flexible but more complicated |
| **Template Method pattern/** | | ~ | Have a template object to parse and export data and extend that general method for pdf, excel, and many more |
| **Adapter pattern/** | | ~ | Create communication between objects with incompatible interfaces (in python with multiple inheritance) - (car-to-rail adapter) |
| **Facade pattern/** | | ~ | Defend our project from external dependencies. When an external dependency updates and needs our implementation to change, we will have one single point to re-implement |
| **Strategy pattern/** | | x | Define a family of algorithms with a base object and change algorithms on run time (public transport - car transport - concept to remember) |
| **State pattern/** | | x | Context - State Interface - States, object change it's behavior on runtime (audio player concept to remember) |
| **Proxy pattern/** | | x | Somewhat like repository pattern, but proxy is for objects and not hiding databases and data sources dependencies |
| **Chain of Responsibility pattern/** | | x | Handler-AbstractHandler and multiple handlers that decides to proccess the request or to pass it to next handler |
| **Bridge pattern/** | | x | Split a large class or a set of closely related classes into two separate hierarchies—abstraction and implementation—which can be developed independently of each other. Warning about dependency between those objects and hierarchies, one object must be creatable independently from the other object-hierarchy (propably the second object-hierarchy will not have meaning to be independently created) |
| **Composite pattern/** | | x | Objects like tree ("Package"-"cellPhone-book" concept to remember) |
| **Decorator pattern/** | | x | (“Wrapper” is the alternative nickname for the Decorator) Attach new behavior in already existing objects by placing wrappers (notify-slack-sms-email concept to remember) |
| **Observer pattern/** | | ~ | Notify subscribers about any change in our object (notify only subscribers that observing) |
| **Builder pattern/** | | x | Build same base objects with different extras on them ("medic robots" concept to remember) |
| **Flyweight pattern/** | | x | 10.000 objects with some common info, keeping common info in one single object (less ram) |
| **Repository pattern/** | | | Have single object to operate with database |

Aggregation vs Composition : 
- Aggregation: object A contains objects B; B can live without A.
- Composition: object A consists of objects B; A manages life cycle of B; B can’t live without A.

This readme file is created for [GitLab](https://gitlab.com/).  

The directories tree, how it will look when you download the repo :
```mermaid
graph TD;
  PTDesignPatterns-->C#;
  PTDesignPatterns-->Python;
  PTDesignPatterns-->readme.md;
  C#-->...;
  Python-->....;
```
