from typing import Dict


class BulletFlyweight:
    """
    """

    def __init__(self, bulletBasicCharectiristics: list) -> None:
        self._privateBulletBasicCharectiristics = bulletBasicCharectiristics

    def operation(self, uniqueState: list) -> None:
        s = self._privateBulletBasicCharectiristics
        u = uniqueState
        print(f"BulletFlyweight: Displaying shared ({s}) and unique ({u}) state.", end="")


class FlyweightFactory:
    """
    """

    _flyweights: Dict[str, BulletFlyweight] = {}

    def __init__(self, initialFlyweights: list) -> None:
        for state in initialFlyweights:
            self._flyweights[self.getHashForObject(state)] = BulletFlyweight(state)

    def getHashForObject(self, state: list) -> str:
        """Warning: maybe pycharm or any other
        IDE for python may show a warning here that the user
        can make this function static. DO NOT.
        Static function are made to implement generic behavior and not specific
        object behavior and in this situation the implementation must
        be very specific for this situation"""

        return "_".join(sorted(state))

    def getFlyweightObjectReference(self, sharedState: list) -> BulletFlyweight:
        """
        Returns an existing Flyweight with a given state or creates a new one.
        """

        key = self.getHashForObject(sharedState)

        if not self._flyweights.get(key):
            print("FlyweightFactory: Can't find a flyweight, creating new one.")
            self._flyweights[key] = BulletFlyweight(sharedState)
        else:
            print("FlyweightFactory: Reusing existing flyweight.")

        return self._flyweights[key]

    def showFlyweights(self) -> None:
        count = len(self._flyweights)
        print(f"FlyweightFactory: I have {count} flyweights:")
        print("\n".join(map(str, self._flyweights.keys())), end="")
