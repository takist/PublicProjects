from Flyweight import FlyweightFactory


def addABulletInSpace(givenFactory: FlyweightFactory, bulletType: str,
                      bulletSpray: str, x: str, y: str, z: str,
                      xVector: str, yVector: str, zVector: str) -> None:
    print("\n\nClient: Adding a bullet in space.")
    flyweight = givenFactory.getFlyweightObjectReference([bulletType, bulletSpray])
    flyweight.operation([x, y, z, xVector, yVector, zVector])


if __name__ == "__main__":

    factory = FlyweightFactory([
        ["6mm", "grey"],
        ["12mm", "black"],
        ["45mm", "grey"],
        ["66mm", "lightGrey"]
    ])

    factory.showFlyweights()

    addABulletInSpace(
        factory, "12mm", "black", "2", "5", "6", "2,5", "5,5", "6,5")

    addABulletInSpace(
        factory, "35mm", "black", "2", "5", "6", "6,5", "-5,5", "-3,5")

    print("\n")

    factory.showFlyweights()
