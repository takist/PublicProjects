from IDatabase import IDatabase
from DatabaseQuiring import DatabaseQuiring
from ReturnedData import ReturnedAllDataObject


class DatabaseProxy(IDatabase):
    """
    The Proxy has an interface identical to the RealSubject.
    """

    def __init__(self, real_subject: DatabaseQuiring) -> None:
        self._real_subject = real_subject

    def GetAllFromSingleTable(self, givenTableName: str) -> ReturnedAllDataObject:
        """
        The most common applications of the Proxy pattern are lazy loading,
        caching, controlling the access, logging, etc. A Proxy can perform one
        of these things and then, depending on the result, pass the execution to
        the same method in a linked RealSubject object.
        """

        if self.check_access():
            returned = self._real_subject.GetAllFromSingleTable("AllVulnerabilities")
            self.log_access()
            return returned
        else:
            return ReturnedAllDataObject()

    def check_access(self) -> bool:
        print("Proxy: Checking access prior to firing a real request.")
        return True

    def log_access(self) -> None:
        print("Proxy: Logging the time of request.", end="")
