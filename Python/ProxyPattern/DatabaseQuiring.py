from IDatabase import IDatabase
from ReturnedData import ReturnedAllDataObject
import datetime


class DatabaseQuiring(IDatabase):
    """
    The RealSubject contains some core business logic. Usually, RealSubjects are
    capable of doing some useful work which may also be very slow or sensitive -
    e.g. correcting input data. A Proxy can solve these issues without any
    changes to the RealSubject's code.
    """

    def GetAllFromSingleTable(self, givenTableName: str) -> ReturnedAllDataObject:
        print(f"DatabaseQuiring: Handling query request for table: {givenTableName}")
        returned = ReturnedAllDataObject()
        returned.successful = True
        returned.dataList = [1, 2, 3]
        returned.timeStamp = datetime.datetime.utcnow()
        return returned
