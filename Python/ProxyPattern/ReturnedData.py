import datetime


class ReturnedAllDataObject:

    def __init__(self):
        self.successful = False
        self.dataList = []
        self.errorMessage = ""
        self.errorObject = None
        self.timeStamp = datetime.datetime(1, 1, 1, 1, 1, 1, 1)
