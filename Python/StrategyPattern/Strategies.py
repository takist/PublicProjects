from abc import ABC, abstractmethod
from typing import List


class Strategy(ABC):
    """
    """

    @abstractmethod
    def findRoute(self, waypoints: List):
        if len(waypoints) < 2:
            raise Exception("List of waypoints must be at least 2 (start point and destination)")


class TravelWithPublicTransport(Strategy):
    def findRoute(self, waypoints: List) -> List:
        super().findRoute(waypoints)
        return sorted(waypoints)


class TravelWithCar(Strategy):
    def findRoute(self, waypoints: List) -> List:
        super().findRoute(waypoints)
        temp = reversed(sorted(waypoints))
        temp = list(temp)
        return temp
