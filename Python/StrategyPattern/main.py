from __future__ import annotations
from Strategies import Strategy, TravelWithPublicTransport, TravelWithCar


class Context:
    """
    """

    def __init__(self, strategy: Strategy, givenWaypoints: list) -> None:
        self._waypoints = givenWaypoints
        self._strategy = strategy

    @property
    def strategy(self) -> Strategy:
        return self._strategy

    @strategy.setter
    def strategy(self, strategy: Strategy) -> None:
        self._strategy = strategy

    def showRoute(self) -> None:
        result = self._strategy.findRoute(self._waypoints)
        print(",".join(result))


if __name__ == "__main__":

    print("Public Transportation: ", end="")
    context = Context(TravelWithPublicTransport(), ["a", "b"])
    context.showRoute()

    print("Car Transportation: ", end="")
    context.strategy = TravelWithCar()
    context.showRoute()

    # Code must throw an error here
    print("Public Transportation: ", end="")
    context = Context(TravelWithPublicTransport(), ["c"])
    context.showRoute()
