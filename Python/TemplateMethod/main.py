import AbstractFileAlgorithm, ConcreteFileAlgorithms


def client_code(abstract_class: AbstractFileAlgorithm.AbstractFileAlgorithm) -> None:
    """
    The client code calls the template method to execute the algorithm. Client
    code does not have to know the concrete class of an object it works with, as
    long as it works with objects through the interface of their base class.
    """

    # ...
    abstract_class.template_method()
    # ...


if __name__ == "__main__":
    print("Same client code can work with different subclasses:")
    client_code(ConcreteFileAlgorithms.WordFileAlgorithm())
    print("")

    print("Same client code can work with different subclasses:")
    client_code(ConcreteFileAlgorithms.PdfFileAlgorithm())
