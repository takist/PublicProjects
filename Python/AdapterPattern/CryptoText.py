from PlainText import PlainText


class CryptedText:
    """
    The Crypted text contains some useful behavior, but its interface is incompatible
    with the existing client code. The Crypted text needs some adaptation before the
    client code can use it.

    We want "request" as method
    """

    def specific_request(self) -> str:
        return ".eetpadA eht fo roivaheb laicepS"


class Adapter(PlainText, CryptedText):
    """
    The Adapter makes the CryptedText interface compatible with the PlainText's
    interface via multiple inheritance.

    Actually we need an adepter for every new implementation
    """

    def request(self) -> str:
        return f"Adapter: (TRANSLATED) {self.specific_request()[::-1]}"
