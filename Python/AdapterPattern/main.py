from CryptoText import CryptedText, Adapter
from PlainText import PlainText


def client_code(target: "PlainText") -> None:
    """
    The client code supports all classes that follow the Target interface.
    """

    print(target.request(), end="")


if __name__ == "__main__":
    print("Client: I can work just fine with the default PlainText objects:")
    target = PlainText()
    client_code(target)
    print("\n")

    adaptee = CryptedText()
    print("Client: The Crypted Text class has a weird interface. "
          "See, I don't understand it:")
    print(f"Crypted Text: {adaptee.specific_request()}", end="\n\n")

    print("Client: But I can work with it via the Adapter:")
    adapter = Adapter()
    client_code(adapter)
