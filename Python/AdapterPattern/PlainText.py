
class PlainText:
    """
    Defines the domain-specific interface used by the client code.

    We want to print plain text and have always "request" as a method
    """

    def request(self) -> str:
        return "Plain Text: The default target's behavior."
