from Interfaces import IMyNotifier, IObserver


class ConcreteObserverA(IObserver):
    def update(self, myNotify: IMyNotifier) -> None:
        if myNotify._state < 3:
            print("ConcreteObserverA: Reacted to the event")


class ConcreteObserverB(IObserver):
    def update(self, myNotify: IMyNotifier) -> None:
        if myNotify._state == 0 or myNotify._state >= 2:
            print("ConcreteObserverB: Reacted to the event")
