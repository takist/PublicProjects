from InsideSystem import MyNotifier
from OutsideSystem import ConcreteObserverA, ConcreteObserverB


if __name__ == "__main__":
    # The client code.

    subject = MyNotifier()

    observer_a = ConcreteObserverA()
    subject.attach(observer_a)

    observer_b = ConcreteObserverB()
    subject.attach(observer_b)

    subject.some_business_logic()
    subject.some_business_logic()

    subject.detach(observer_a)

    subject.some_business_logic()
