from SendSmsNotification import ISendSms, SendSms
from NotificationExtend import SendEmail, SendWhatsUpNotification


def client_code(component: ISendSms) -> None:
    """
    The client code works with all objects using the Component interface. This
    way it can stay independent of the concrete classes of components it works
    with.
    """

    print(f"RESULT: {component.operation()}", end="")


if __name__ == "__main__":
    # This way the client code can support both simple components...
    simple = SendSms()
    print("Client: I've got a simple component:")
    client_code(simple)
    print("\n")

    # ...as well as decorated ones.
    #
    # Note how decorators can wrap not only simple components but the other
    # decorators as well.
    decorator1 = SendEmail(simple)
    decorator2 = SendWhatsUpNotification(decorator1)
    print("Client: Now I've got a decorated component:")
    client_code(decorator2)
