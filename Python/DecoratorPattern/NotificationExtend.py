from SmsDecorator import Decorator


class SendEmail(Decorator):
    """
    Concrete Decorators call the wrapped object and alter its result in some
    way.
    """

    def operation(self) -> str:
        """
        Decorators may call parent implementation of the operation, instead of
        calling the wrapped object directly. This approach simplifies extension
        of decorator classes.
        """
        return f"Email Send and also -> ({self.component.operation()})"


class SendWhatsUpNotification(Decorator):
    """
    Decorators can execute their behavior either before or after the call to a
    wrapped object.
    """

    def operation(self) -> str:
        return f"Whats'up notification send and also -> ({self.component.operation()})"
