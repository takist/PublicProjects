from GeneralPackage import GeneralPackage


class CellPhone(GeneralPackage):
    def __init__(self, price: int):
        super(CellPhone, self).__init__(price)
        self._extraTaxes = 30

    def isPackage(self) -> bool:
        return False

    def getPrice(self) -> int:
        return self._price + self._extraTaxes


class Book(GeneralPackage):
    def isPackage(self) -> bool:
        return False

    def getPrice(self) -> int:
        return self._price


class Chair(GeneralPackage):
    def isPackage(self) -> bool:
        return False

    def getPrice(self) -> int:
        return self._price
