from typing import List
from GeneralPackage import GeneralPackage
from CompositePricedObjects import Chair, CellPhone, Book


class PackageWithMany(GeneralPackage):

    def __init__(self):
        super().__init__()
        self._children: List[GeneralPackage] = []

    def add(self, component: GeneralPackage) -> None:
        self._children.append(component)
        component.parent = self

    def remove(self, component: GeneralPackage) -> None:
        self._children.remove(component)
        component.parent = None

    def isPackage(self) -> bool:
        return True

    def getPrice(self) -> str:
        """
        The Composite executes its primary logic in a particular way. It
        traverses recursively through all its children, collecting and summing
        their results. Since the composite's children pass these calls to their
        children and so forth, the whole object tree is traversed as a result.
        """

        results = []
        for child in self._children:
            results.append(str(child.getPrice()))
        return f"Branch({'+'.join(results)})"


def client_code(component: GeneralPackage) -> None:
    print(f"RESULT: {component.getPrice()}", end="")


if __name__ == "__main__":

    cellPhoneNokia = CellPhone(400)
    bookSOLIDPrinciples = Book(35)
    chairForProgramming = Chair(155)

    print("Client: I've got a simple component:")
    client_code(cellPhoneNokia)
    print("\n")

    tree = PackageWithMany()

    branch1 = PackageWithMany()
    branch1.add(bookSOLIDPrinciples)
    branch1.add(chairForProgramming)

    branch2 = PackageWithMany()
    branch2.add(cellPhoneNokia)

    tree.add(branch1)
    tree.add(branch2)

    print("Client: Now I've got a composite tree:")
    client_code(tree)
    print("\n")
