from __future__ import annotations
from abc import ABC, abstractmethod


class GeneralPackage(ABC):

    def __init__(self, price: int = None):
        self._price = price

    @property
    def parent(self) -> GeneralPackage:
        return self._parent

    @parent.setter
    def parent(self, parent: GeneralPackage):
        self._parent = parent

    def add(self, component: GeneralPackage) -> None:
        pass

    def remove(self, component: GeneralPackage) -> None:
        pass

    @abstractmethod
    def isPackage(self) -> bool:

        """ Is not a composite package, or in our tree logic, it's not a leaf.
            Otherwise this function should return False"""

        return True

    @abstractmethod
    def getPrice(self) -> int:
        
        """ Returns the price of the composite package or all prices added together
            inside """

        pass
