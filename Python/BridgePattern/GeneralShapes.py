from abc import ABC, abstractmethod
from GeneralColoring import Coloring


class GeneralShape(ABC):
    def __init__(self, colorImplementation: Coloring = None) -> None:
        self.colorImplementation = colorImplementation

    @abstractmethod
    def showPaintedShape(self) -> str:
        if self.colorImplementation is None:
            return "None"
        else:
            return self.colorImplementation.paintTheShape()


class Circle(GeneralShape):
    def showPaintedShape(self) -> str:
        showing = f"Circle shape with: {super().showPaintedShape()} color"
        return showing


class Square(GeneralShape):
    def showPaintedShape(self) -> str:
        showing = f"Square shape with: {super().showPaintedShape()} color"
        return showing
