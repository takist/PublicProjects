from abc import ABC, abstractmethod


class Coloring(ABC):

    @abstractmethod
    def paintTheShape(self) -> str:
        pass


class RedColor(Coloring):
    def paintTheShape(self) -> str:
        return "Red"


class BlueColor(Coloring):
    def paintTheShape(self) -> str:
        return "Blue"
