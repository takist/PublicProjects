from __future__ import annotations
from GeneralShapes import GeneralShape, Circle, Square
from GeneralColoring import RedColor, BlueColor


def client_code(generalShape: GeneralShape) -> None:
    """
    """

    print(generalShape.showPaintedShape(), end="")


if __name__ == "__main__":

    color = RedColor()
    shapeWithColor = Circle(color)
    client_code(shapeWithColor)

    print("\n")

    color = BlueColor()
    shapeWithColor = Square(color)
    client_code(shapeWithColor)

    print("\n")

    shapeWithNoneColor = Circle()
    client_code(shapeWithNoneColor)
