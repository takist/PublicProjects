import CreatorPattern


def client_code(creator: CreatorPattern.CreateCellPhone) -> None:

    print(f"Client: I'm not aware of the creator's class, but it still works.\n"
          f"{creator.some_operation()}", end="")


if __name__ == '__main__':
    print("App: Launched with the CreateMotorollaCellPhone.")
    client_code(CreatorPattern.CreateMotorollaCellPhone())
    print("\n")

    print("App: Launched with the CreateLGCellPhone.")
    client_code(CreatorPattern.CreateLGCellPhone())
else:
    print("Not run as main!")
