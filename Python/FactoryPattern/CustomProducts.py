from abc import ABC, abstractmethod


class CellPhone(ABC):

    ModelName = None
    SARValue = None
    Ram = None
    Storage = None
    CPU = None
    Inches = None
    Rating = None

    def __init__(self, givenModel: str, sar: str, ram: str, stor: str, cpu: str, inches: str):
        self.ModelName = givenModel
        self.SARValue = sar
        self.Ram = ram
        self.Storage = stor
        self.CPU = cpu
        self.Inches = inches

    @abstractmethod
    def doRate(self) -> str:
        # Let's imagine that every company with cell-phones has it's own
        # algorithm to create the final rating of the cell phone
        pass


class Motorolla(CellPhone):

    SpecialMotorollaCharacteristic = None

    def __init__(self, specialMot: str, givenModel: str, sar: str, ram: str, stor: str, cpu: str, inches: str):
        super().__init__(givenModel, sar, ram, stor, cpu, inches)
        self.SpecialMotorollaCharacteristic = specialMot

    def doRate(self) -> str:
        return f"[New Motorola Cell-Phone, rate: {self.CPU + self.Ram} and special characteristic : {self.SpecialMotorollaCharacteristic}]"


class LG(CellPhone):

    def doRate(self) -> str:
        return f"[New LG Cell-Phone, rate: {self.CPU + self.Ram}]"
