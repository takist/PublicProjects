from abc import ABC, abstractmethod
import CustomProducts


class CreateCellPhone(ABC):

    @abstractmethod
    def factory_method(self):
        pass

    def some_operation(self) -> str:

        # Call the factory method to create a Product object.
        product = self.factory_method()

        # Now, use the product.
        result = f"Creator: The same creator's code has just worked for a cell phone with rate {product.DoRate()}"

        return result


class CreateMotorollaCellPhone(CreateCellPhone):
    def factory_method(self) -> CustomProducts.CellPhone:
        return CustomProducts.Motorolla()
