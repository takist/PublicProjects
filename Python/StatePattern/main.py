from BaseState import AudioPlayerContext
from States import StoppedState

if __name__ == "__main__":

    audioPlayer = AudioPlayerContext(StoppedState())
    audioPlayer.StopButton()
    audioPlayer.PlayButton()
    audioPlayer.StopButton()
    audioPlayer.PlayButton()
    audioPlayer.LockButton()
    audioPlayer.StopButton()
    audioPlayer.PlayButton()
    audioPlayer.LockButton()
