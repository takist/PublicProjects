from __future__ import annotations
from abc import ABC, abstractmethod


class AudioPlayerContext:
    """
    The Context defines the interface of interest to clients. It also maintains
    a reference to an instance of a State subclass, which represents the current
    state of the Context.
    """

    _state = None

    """
    A reference to the current state of the Context.
    """

    def __init__(self, state: State) -> None:
        self.transition_to(state)

    def transition_to(self, state: State):
        """
        The Context allows changing the State object at runtime.
        """

        print(f"AudioPlayerContext: Transition to {type(state).__name__}")
        self._state = state
        self._state.context = self

    """
    The Context delegates part of its behavior to the current State object.
    """

    def PlayButton(self):
        self._state.playB()

    def StopButton(self):
        self._state.stopB()

    def LockButton(self):
        self._state.lockB()


class State(ABC):
    """
    The base State class declares methods that all Concrete State should
    implement and also provides a backreference to the Context object,
    associated with the State. This backreference can be used by States to
    transition the Context to another State.
    """

    _lastStateIsPlay = None

    @property  # this used in python like "getter"
    def context(self) -> AudioPlayerContext:
        return self._context

    @context.setter  # this used in python like "setter"
    def context(self, context: AudioPlayerContext) -> None:
        self._context = context

    @abstractmethod
    def playB(self) -> None:
        pass

    @abstractmethod
    def stopB(self) -> None:
        pass

    @abstractmethod
    def lockB(self) -> None:
        pass
