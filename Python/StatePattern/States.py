from BaseState import State


class PlayingState(State):
    def __init__(self) -> None:
        # This boolean we need it only for locked state just to know
        # when the players unlock the audio player
        # we must move to the state that we was before locking
        self._lastStateIsPlay = True
        print("PlayingState: La La La La")

    def playB(self) -> None:
        print("PlayingState: Player already plays, what are you doing?")

    def stopB(self) -> None:
        print("PlayingState: Player stops.")
        self.context.transition_to(StoppedState())

    def lockB(self) -> None:
        print("PlayingState: Player keeps playing but it's gonna lock while playing!")
        self.context.transition_to(LockedState(self._lastStateIsPlay))


class LockedState(State):
    def __init__(self, givenLastPlaying: bool) -> None:
        self._lastStateIsPlay = givenLastPlaying
        print("LockedState: Player just locked.")

    def playB(self) -> None:
        print("LockedState: Player can not operate, is locked.")

    def stopB(self) -> None:
        print("LockedState: Player can not operate, is locked.")

    def lockB(self) -> None:
        if self._lastStateIsPlay:
            print("LockedState: Player unlocking but continues to play.")
            self.context.transition_to(PlayingState())
        else:
            print("LockedState: Player unlocking and returns to stopped as was before locking.")
            self.context.transition_to(StoppedState())


class StoppedState(State):
    def __init__(self) -> None:
        self._lastStateIsPlay = False
        print("StoppedState: Just stopped playing.")

    def playB(self) -> None:
        print("StoppedState: Starting to play")
        self.context.transition_to(PlayingState())

    def stopB(self) -> None:
        print("StoppedState: Player is already stopped, what are you doing?")

    def lockB(self) -> None:
        print("StoppedState: Player locking and it's not playing")
        self.context.transition_to(LockedState(self._lastStateIsPlay))
