from __future__ import annotations
from typing import Any


class FirstAidRobot:

    def __init__(self) -> None:
        self.abilities = []

    def add(self, part: Any) -> None:
        self.abilities.append(part)

    def list_parts(self) -> None:
        print(f"First Aid Robot abilities: {', '.join(self.abilities)}", end="")
