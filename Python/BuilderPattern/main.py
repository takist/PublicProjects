from __future__ import annotations
from Builders.IRobotBuilder import IRobotBuilder
from Builders.RobotBuilder import HealthCareRobotBuilder


class Director:
    """
    Optional
    """

    def __init__(self) -> None:
        self._builder = None

    @property
    def builder(self) -> IRobotBuilder:
        return self._builder

    @builder.setter
    def builder(self, builder: IRobotBuilder) -> None:
        self._builder = builder

    def buildMinimalFirstAidRobot(self) -> None:
        self.builder.constructBasicFirstAid()

    def buildFullFirstAidRobot(self) -> None:
        self.builder.constructBasicFirstAid()
        self.builder.constructPoisonAntidotes()
        self.builder.constructFullHealthChecks()


if __name__ == "__main__":

    director = Director()
    builder = HealthCareRobotBuilder()
    director.builder = builder

    print("Standard health care robot: ")
    director.buildMinimalFirstAidRobot()
    builder.robot.list_parts()

    print("\n")

    print("Full featured health care robot: ")
    director.buildFullFirstAidRobot()
    builder.robot.list_parts()

    print("\n")

    # Builder pattern can be used without a Director class
    print("Custom health care robot: ")
    builder.constructBasicFirstAid()
    builder.constructPoisonAntidotes()
    builder.robot.list_parts()
