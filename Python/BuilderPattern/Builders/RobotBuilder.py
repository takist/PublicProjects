from __future__ import annotations
from Builders.IRobotBuilder import IRobotBuilder
from FirstAidRobot import FirstAidRobot


class HealthCareRobotBuilder(IRobotBuilder):
    """
    We can have several variations of Builders
    """

    def __init__(self) -> None:
        self.reset()

    def reset(self) -> None:
        self._robot = FirstAidRobot()

    @property
    def robot(self) -> FirstAidRobot:
        temp = self._robot
        self.reset()
        return temp

    def constructBasicFirstAid(self) -> None:
        self._robot.add("Bedadine")
        self._robot.add("Bandages")
        self._robot.add("CPR")

    def constructPoisonAntidotes(self) -> None:
        self._robot.add("Snake antidote")
        self._robot.add("Scorpion antidote")
        self._robot.add("Spider antidote")
        self._robot.add("JellyFish-Medusa antidote")

    def constructFullHealthChecks(self) -> None:
        self._robot.add("Full-Body check up for malfunctions")
