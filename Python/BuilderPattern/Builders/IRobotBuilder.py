from __future__ import annotations
from abc import ABC, abstractmethod


class IRobotBuilder(ABC):

    @property
    @abstractmethod
    def robot(self) -> None:
        pass

    @abstractmethod
    def constructBasicFirstAid(self) -> None:
        pass

    @abstractmethod
    def constructPoisonAntidotes(self) -> None:
        pass

    @abstractmethod
    def constructFullHealthChecks(self) -> None:
        pass
