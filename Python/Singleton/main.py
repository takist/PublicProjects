import DbOperations


if __name__ == "__main__":
    print("Creating first object")
    a = DbOperations.DbOperations(5)
    print("Hash of first object: ", hash(a), " and with unique number:", a.variableUnique)
    print("Creating second object")
    b = DbOperations.DbOperations(9)
    print(f'Hash of second object: {hash(b)} and with unique number: {b.variableUnique}')
else:
    print("Not run as Main!!")
