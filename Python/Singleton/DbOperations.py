
class DbOperations:

    _localInstance = None

    # Here we can read our configuration parameters from external source dynamically
    _connectionString = "some connection string to the database"
    _usr = "admin"
    _passw = "keno"

    variableUnique = None

    def __init__(self, x):
        self.variableUnique = x
        # Here we make the connection with the database

    def __new__(cls, x, *args, **kwargs):
        if cls._localInstance is None:
            cls._localInstance = object.__new__(cls)
            cls.variableUnique = x
        return cls

    def __del__(self):
        pass

    def read(self):
        pass

    def delete(self):
        pass

    def add(self):
        pass

    def update(self):
        pass
